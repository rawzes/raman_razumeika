package com.rr.training.hw11;

import java.io.File;
import java.sql.*;
import java.util.Scanner;

public class Runner {
    private static final String TABLE_NAME = "Employees";
    private static final String CONNECTION_PATH = "src/main/resources/db/db_for_m4_l3.db";

    public static void main(String[] args) {

        Connection connection = getNewConnection();

        int userInput = 0;
        do {
            userInput = displayMenu();
            try {
                switch (userInput) {
                    case 1:
                        addEmployee(connection);
                        break;
                    case 2:
                        deleteEmployeeByID(connection);
                        break;
                    case 3:
                        getListOfEmployees(connection);
                        break;
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

        } while (userInput != 4);

        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void getListOfEmployees(Connection connection) throws SQLException {
        Statement statement = connection.createStatement();
        try (ResultSet resultSet = statement.executeQuery(String.format("select * from '%s'", TABLE_NAME))) {
            Employees tmp = new Employees();
            while (resultSet.next()) {
                tmp.addNewEmployee(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3));
            }
            resultSet.close();
            tmp.getListOfEmployees();
        }
        statement.close();
    }

    private static Connection getNewConnection() {
        Connection connection = null;
        File configFile = new File(CONNECTION_PATH.toString());
        System.out.println(CONNECTION_PATH);
        if (configFile.exists()) try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:" + CONNECTION_PATH);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return connection;
    }

    private static void addEmployee(Connection connection) throws SQLException {
        Scanner in = new Scanner(System.in);
        System.out.print("Enter name: ");
        String name = in.nextLine();
        System.out.print("Enter family: ");
        String family = in.nextLine();
        Statement statement = connection.createStatement();
        statement.executeUpdate(String.format("INSERT INTO '%s' ('firstname', 'lastname') VALUES ('%s', '%s')", TABLE_NAME, name, family));
        System.out.println("New employee has been added...");
        statement.close();
    }

    private static void deleteEmployeeByID(Connection connection) throws SQLException {
        Scanner in = new Scanner(System.in);
        System.out.print("Enter ID: ");
        int ID = Integer.parseInt(in.nextLine());
        PreparedStatement statement = connection.prepareStatement(String.format("DELETE FROM '%s' WHERE id = ?", TABLE_NAME));
        statement.setInt(1, ID);
        statement.execute();
        System.out.println("The employee has been deleted...");
        statement.close();
    }

    private static int displayMenu() {
        System.out.println();
        System.out.println("Employees Manager via JDBC");
        System.out.println("1. Add an Employee");
        System.out.println("2. Delete an Employee by ID");
        System.out.println("3. Get list of employees");
        System.out.println("4. Exit");
        System.out.println();
        System.out.print("Enter your choice: ");
        Scanner in = new Scanner(System.in);
        int number = 0;
        try {
            number = Integer.parseInt(in.nextLine());
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return number;
    }
}
