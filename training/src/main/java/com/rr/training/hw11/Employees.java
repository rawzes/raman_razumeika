package com.rr.training.hw11;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Employees implements Serializable {
    private List<Employee> employees = null;

    public Employees(List<Employee> employees) {
        this.employees = employees;
    }

    public Employees() {
        this.employees = new ArrayList<>();
    }

    public void addNewEmployee(int ID, String fName, String lName) {
        Employee employee = new Employee(ID, fName, lName);
        employees.add(employee);
    }


    public void getListOfEmployees() {
        if (!employees.isEmpty()) {
            employees.forEach((employee) -> System.out.println(employee.toString()));
        } else {
            System.out.println("The list is empty...");
        }
    }
}