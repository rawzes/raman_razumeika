package com.rr.training.hw11;

public class Employee {
    private String firstName;
    private String lastName;
    private int ID;

    public Employee(int ID, String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.ID = ID;
    }

    @Override
    public String toString() {
        return String.format("ID: %s, First name: %s, Last name: %s", ID, firstName, lastName);
    }
}

