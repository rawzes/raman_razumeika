package com.rr.training.hw5.task1;

public class Card {
    private String ownerName;
    private double balance;

    public Card(String ownerName, double balance) {
        this.ownerName = ownerName;
        this.balance = balance;
    }

    public Card(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public double getBalance() {
        return this.balance;
    }

    public void addBalance(double sum) {
        if (sum < 0) {
            throw new IllegalArgumentException();
        } else {
            this.balance += sum;
        }

    }

    public void reduceBalance(double sum) {
        if (this.balance - sum < 0) {
            throw new IllegalArgumentException();
        } else {
            this.balance -= sum;
        }
    }

    public double convertToEUR(double course) {
        return this.balance * course;
    }

    public double convertToBYN(double course) {
        return this.balance * course;
    }
}
