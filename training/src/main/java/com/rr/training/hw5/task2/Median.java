package com.rr.training.hw5.task2;

import java.util.Arrays;

public class Median {

    public static float median(int[] array) {
        Arrays.sort(array);
        float median;
        int middle = array.length / 2;
        if (array.length % 2 == 0)
            median = ((float) array[middle] + (float) array[middle - 1]) / 2;
        else
            median = (float) array[middle];
        return median;
    }

    public static double median(double[] array) {
        Arrays.sort(array);
        double median;
        int middle = array.length / 2;
        if (array.length % 2 == 1)
            median = array[middle];
        else
            median = (array[middle - 1] + array[middle]) / 2;
        return median;
    }
}
