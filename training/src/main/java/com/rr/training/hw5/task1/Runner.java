package com.rr.training.hw5.task1;

public class Runner {

    public static void main(String[] args) {

        Card card = new Card("Roma", 500);
        System.out.println("Initial balance: " + card.getBalance());
        System.out.println("Balance in EUR: " + card.convertToEUR(0.98));
        System.out.println("Balance in BYN: " + card.convertToBYN(2.08));

    }
}
