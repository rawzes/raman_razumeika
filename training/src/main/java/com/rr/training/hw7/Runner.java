package com.rr.training.hw7;

import java.util.*;

public class Runner {
    private static final String INPUT_STRING = "Once upon a time a Wolf was lapping at a spring on a hillside, when, looking up, what should he see but a Lamb just beginning to drink a little lower down.";

    public static void main(String[] args) {
        Set<String> setWords = getWordList(INPUT_STRING);
        HashMap<Character, String> map = getMapOfWords(setWords);
        Map<Character, String> treeMap = new TreeMap<>(map);
        printMap(treeMap);
    }

    private static void printMap(Map<Character, String> treeMap) {
        for (Map.Entry<Character, String> entry : treeMap.entrySet())
            System.out.println(entry.getKey().toString().toUpperCase().concat(": ").concat(entry.getValue()));
    }

    private static HashMap<Character, String> getMapOfWords(Set<String> setWords) {
        HashMap<Character, String> map = new HashMap<>();
        for (String setElement : setWords) {
            char firstLetter = setElement.charAt(0);
            if (map.containsKey(firstLetter)) {
                map.put(firstLetter, setElement.concat(" ").concat(map.get(firstLetter)));
            } else {
                map.put(firstLetter, setElement);
            }
        }
        return map;
    }

    private static Set<String> getWordList(String source) {
        return new TreeSet<>(Arrays.asList(source.toLowerCase().replaceAll("[,.]", "").split("\\s")));
    }
}
