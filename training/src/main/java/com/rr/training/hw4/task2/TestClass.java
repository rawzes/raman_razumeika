package com.rr.training.hw4.task2;


public class TestClass {
    static int staticIntVariable;
    static boolean staticBooleanVariable;

    static {
        staticIntVariable = 5;
        System.out.println("Static initalization: staticVariable is: " + staticIntVariable);
    }

    static {
        staticBooleanVariable = true;
        System.out.println("Static initalization: staticBooleanVariable is: " + staticBooleanVariable);
    }

    public int nonStaticIntVariable;
    public String nonStaticStringVariable;

    {
        nonStaticIntVariable = 6;
        System.out.println("Non static initalization: nonStaticIntVariable is: " + nonStaticIntVariable);
    }

    {
        nonStaticStringVariable = "test String";
        System.out.println("Non static initalization: nonStaticStringVariable is: " + nonStaticStringVariable);
    }


    public TestClass() {
        System.out.println("Constructor.");
    }
}
