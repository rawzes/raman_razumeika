package com.rr.training.hw4.tesk3;

import java.math.BigInteger;

public class Recursion {
    private static final int maxIntValue = Integer.MAX_VALUE;
    private static final long maxLongValue = Long.MAX_VALUE;

    public static void main(String[] args) {
        int countInteger = getMaxNumberForInteger(maxIntValue);
        int countLong = getMaxNumberForLong(maxLongValue);
        System.out.println("The number of elements for INTEGER: " + countInteger);
        System.out.println("The number of elements for LONG: " + countLong);
    }

    private static int getMaxNumberForInteger(int maxValue) {
        int count = 0;
        while (true) {
            long number = getFibForInt(count);
            if (number > maxValue) {
                break;
            } else {
                count++;
            }
        }
        return count;
    }

    private static long getFibForInt(int n) {
        if (n == 0) {
            return 0;
        } else if (n == 1) {
            return 1;
        } else if (n == 2) {
            return 1;
        } else
            return getFibForInt(n - 1) + getFibForInt(n - 2);
    }

    private static int getMaxNumberForLong(long maxValue) {
        Fibonacci fibonacci = new Fibonacci();
        BigInteger limit = BigInteger.valueOf(maxValue);
        int counter = 0;
        while ((fibonacci.next()).compareTo(limit) < 0) {
            counter++;
        }
        return counter;
    }
}
