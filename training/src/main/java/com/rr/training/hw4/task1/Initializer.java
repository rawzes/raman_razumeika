package com.rr.training.hw4.task1;

public class Initializer {

    private static boolean varBoolean;
    private static char varChar;
    private static byte varByte;
    private static short varShort;
    private static int varInt;
    private static long varLong;
    private static float varFloat;
    private static double varDouble;
    private static String varString;

    public static void main(String[] args) {
        System.out.println("Default boolean value: " + varBoolean);
        System.out.println("Default char value: " + varChar);
        System.out.println("Default byte value: " + varByte);
        System.out.println("Default short value: " + varShort);
        System.out.println("Default int value: " + varInt);
        System.out.println("Default long value: " + varLong);
        System.out.println("Default double value: " + varDouble);
        System.out.println("Default String value: " + varString);

    }
}
