package com.rr.training.hw4.tesk3;

import java.math.BigInteger;

public class Fibonacci {
    private BigInteger fib1 = BigInteger.valueOf(1);
    private BigInteger fib0 = BigInteger.valueOf(0);

    public BigInteger next() {
        BigInteger fib2 = fib1.add(fib0);
        fib1 = fib0;
        fib0 = fib2;
        return fib0;
    }
}

