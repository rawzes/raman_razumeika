package com.rr.training.hw9.task1;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class Task1Runner {
    public static void main(String[] args) {
        Integer[] testArray = new Integer[]{101, 21, 300, 4, 55, 5, 1};
        List<Integer> list = Arrays.asList(testArray);
        System.out.println("Before: ");
        System.out.println(list.toString());

        Comparator<Integer> comparator = Comparator.comparing(number -> getSumOfNumbers(number));
        list.sort(comparator);
        System.out.println("After: ");
        System.out.println(list.toString());
    }

    public static int getSumOfNumbers(int number) {
        int sum = 0;
        while (number > 0) {
            sum = sum + number % 10;
            number = number / 10;
        }
        return sum;
    }
}
