package com.rr.training.hw9.task2;

import java.util.Arrays;

public class Task2Runner {
    public static void main(String[] args) {

        OperationSet<Integer> setA = new OperationSet<>(Arrays.asList(0, 2, 4, 5, 6, 8, 10));
        OperationSet<Integer> setB = new OperationSet<>(Arrays.asList(5, 6, 7, 8, 9, 10));

        System.out.println("Union set:");
        OperationSet<Integer> resultSet = setA.getUnionSet(setB);
        System.out.println(resultSet.toString());

        System.out.println("Intersection set:");
        resultSet = setA.getIntersectionSet(setB);
        System.out.println(resultSet.toString());

        System.out.println("Difference set:");
        resultSet = setA.getDifferenceSet(setB);
        System.out.println(resultSet.toString());

        System.out.println("Symmetric difference set:");
        resultSet = setA.getSymmetricDifferenceSet(setB);
        System.out.println(resultSet.toString());
    }
}
