package com.rr.training.hw9.task2;

import java.util.*;

public class OperationSet<T> {
    private Set<T> elementsSet;

    public OperationSet(List<T> elementsList) {
        this.elementsSet = new HashSet<T>(elementsList);
    }

    public OperationSet(Set<T> elementsList) {
        this.elementsSet = new HashSet<T>(elementsList);
    }

    public Set<T> getElementsSet() {
        return elementsSet;
    }


    public OperationSet<T> getUnionSet(OperationSet<T> b) {
        Set<T> tmp = new HashSet<>(this.elementsSet);
        tmp.addAll(b.getElementsSet());
        return new OperationSet<T>(tmp);
    }

    public OperationSet<T> getIntersectionSet(OperationSet<T> b) {
        Set<T> tmp = new HashSet<>(this.elementsSet);
        tmp.retainAll(b.getElementsSet());
        return new OperationSet<T>(tmp);
    }

    public OperationSet<T> getDifferenceSet(OperationSet<T> b) {
        Set<T> tmp = new HashSet<>(this.elementsSet);
        tmp.removeAll(b.getElementsSet());
        return new OperationSet<T>(tmp);
    }

    public OperationSet<T> getSymmetricDifferenceSet(OperationSet<T> b) {
        Set<T> tmpA = new HashSet<>(this.elementsSet);
        Set<T> tmpB = new HashSet<>(b.getElementsSet());

        tmpA.removeAll(b.getElementsSet());
        tmpB.removeAll(this.elementsSet);
        tmpA.addAll(tmpB);
        return new OperationSet<T>(tmpA);
    }
    @Override
    public String toString() {
        return Arrays.asList(elementsSet.toArray()).toString();
    }

}
