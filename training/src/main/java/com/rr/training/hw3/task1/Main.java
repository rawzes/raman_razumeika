package com.rr.training.hw3.task1;

public class Main {
    public static void main(String[] args) {
        if (args.length != 4) {
            throw new IllegalArgumentException("Incorrect number of arguments");
        }
        int a, p;
        double m1, m2, g;
        try {
            a = Integer.parseInt(args[0]);
            p = Integer.parseInt(args[1]);
            m1 = Double.parseDouble(args[2]);
            m2 = Double.parseDouble(args[3]);
            g = (4 * Math.pow(Math.PI, 2) * Math.pow(a, 3)) / (Math.pow(p, 2) * (m1 + m2));
            System.out.println("G = " + g);
        } catch (Exception e) {
            e.getStackTrace();
        }
    }
}
