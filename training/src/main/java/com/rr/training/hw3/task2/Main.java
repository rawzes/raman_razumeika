package com.rr.training.hw3.task2;

public class Main {

    public static void main(String[] args) {

        if (args.length != 3) {
            throw new IllegalArgumentException("Incorrect number of arguments");
        }
        int algorithmId = 0, loopType = 0, number = 0;
        try {
            algorithmId = Integer.parseInt(args[0]);
            loopType = Integer.parseInt(args[1]);
            number = Integer.parseInt(args[2]);
        } catch (Exception e) {
            e.printStackTrace();
        }
        switch (algorithmId) {
            case 1:
                getFibonacci(number, loopType);
                break;
            case 2:
                getFactorial(number, loopType);
                break;
            default:
                System.out.println("Incorrect algorithm ID");
        }
    }

    private static void getFactorial(int number, int loopType) {
        int i = 1, fact = 1;
        switch (loopType) {
            case 1:
                while (i <= number) {
                    fact = fact * i++;
                }
                break;
            case 2:
                do {
                    fact = fact * i++;
                } while (i <= number);
                break;
            case 3:
                for (i = 1; i <= number; i++) {
                    fact = fact * i;
                }
                break;
            default:
                throw new IllegalArgumentException("Incorrect loop type");
        }
        System.out.println("Factorial for " + number + " is = " + fact);
    }

    private static void getFibonacci(int number, int loopType) {
        int previousNumber = 0;
        int nextNumber = 1;
        int i = 1;
        System.out.print("Fibonacci Series of " + number + " numbers: ");
        switch (loopType) {
            case 1:
                while (i <= number) {
                    System.out.print(previousNumber + " ");
                    int sum = previousNumber + nextNumber;
                    previousNumber = nextNumber;
                    nextNumber = sum;
                    i++;
                }
                break;
            case 2:
                do {
                    System.out.print(previousNumber + " ");
                    int sum = previousNumber + nextNumber;
                    previousNumber = nextNumber;
                    nextNumber = sum;
                    i++;
                }
                while (i <= number);
                break;
            case 3:
                for (i = 1; i <= number; ++i) {
                    System.out.print(previousNumber + " ");
                    int sum = previousNumber + nextNumber;
                    previousNumber = nextNumber;
                    nextNumber = sum;
                }
                break;
            default:
                throw new IllegalArgumentException("Incorrect loop type");
        }
    }
}
