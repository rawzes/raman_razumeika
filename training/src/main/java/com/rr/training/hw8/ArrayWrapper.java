package com.rr.training.hw8;

public class ArrayWrapper<T> {
    private T[] array;

    public ArrayWrapper(T[] array) {
        this.array = array;
    }

    public T get(int index) throws IncorrectArrayWrapperIndex {
        if (index < 1 || index > array.length) {
            throw new IncorrectArrayWrapperIndex("Incorrect index");
        }
        return array[index - 1];
    }

    public boolean replace(int index, T value) throws IncorrectArrayWrapperIndex {
        boolean flag = false;
        if (index < 1 || index > array.length) {
            throw new IncorrectArrayWrapperIndex("Incorrect index");
        } else {
            if (array instanceof Integer[]) {
                if ((int) array[index - 1] < (int) value) {
                    array[index - 1] = value;
                    flag = true;
                } else {
                    flag = false;
                }
            } else if (array instanceof String[]) {
                if (array[index].toString().length() < value.toString().length()) {
                    array[index - 1] = value;
                    flag = true;
                } else {
                    flag = false;
                }
            } else {
                array[index - 1] = value;
            }
        }
        return flag;
    }
}

