package com.rr.training.hw10;

import java.io.Serializable;

public class Employee implements Serializable {
    private String firstName;
    private String lastName;
    private int ID;

    public Employee(int ID, String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.ID = ID;
    }

    public int getEmpID() {
        return this.ID;
    }

    @Override
    public String toString() {
        return String.format("ID: %s, First name: %s, Last name: %s", ID, firstName, lastName);
    }
}
