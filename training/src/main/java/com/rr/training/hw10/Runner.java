package com.rr.training.hw10;

import java.io.*;
import java.util.Scanner;

public class Runner {
    private static final String pathToConfig = "config.bin";

    public static void main(String[] args) {
        Employees empList = new Employees();
        File configFile = new File(pathToConfig);
        if (configFile.exists()) {
            try {
                FileInputStream fis = new FileInputStream(pathToConfig);
                ObjectInputStream ois = new ObjectInputStream(fis);
                empList = (Employees) ois.readObject();
                ois.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        int userInput = 0;
        do {
            userInput = displayMenu();
            switch (userInput) {
                case 1:
                    addEmployee(empList);
                    break;
                case 2:
                    deleteEmployeeByID(empList);
                    break;
                case 3:
                    empList.getListOfEmployees();
                    break;
                case 4:
                    saveAppState(empList);
                    break;
            }
        } while (userInput != 4);
    }

    private static void addEmployee(Employees emp) {
        Scanner in = new Scanner(System.in);
        System.out.print("Enter name: ");
        String name = in.nextLine();
        System.out.print("Enter family: ");
        String family = in.nextLine();
        emp.addNewEmployee(name, family);
        System.out.println("New employee has been added...");
    }

    private static void deleteEmployeeByID(Employees emp) {
        Scanner in = new Scanner(System.in);
        System.out.print("Enter ID: ");
        int ID = Integer.parseInt(in.nextLine());
        emp.deleteEmployeeByID(ID);
        System.out.println("The employee has been deleted...");
    }

    private static void saveAppState(Employees empList) {
        File configFile = new File(pathToConfig);
        if (configFile.exists()) {

        } else {
            try {
                configFile.createNewFile();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {
            FileOutputStream fis = new FileOutputStream(pathToConfig);
            ObjectOutputStream ois = new ObjectOutputStream(fis);
            ois.writeObject(empList);
            ois.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static int displayMenu() {
        System.out.println("Employees Manager");
        System.out.println("1. Add an Employee");
        System.out.println("2. Delete an Employee by ID");
        System.out.println("3. Get list of employees");
        System.out.println("4. Exit");
        System.out.println();
        System.out.print("Enter your choice: ");
        Scanner in = new Scanner(System.in);
        return Integer.parseInt(in.nextLine());
    }
}
