package com.rr.training.hw10;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Employees implements Serializable {
    private List<Employee> employees = null;

    public Employees(List<Employee> employees) {
        this.employees = employees;
    }

    public Employees() {
        this.employees = new ArrayList<>();
    }

    public void addNewEmployee(String fName, String lName) {
        int nextEmpID = 1;
        if (!employees.isEmpty()) {
            nextEmpID = employees.get(employees.size() - 1).getEmpID() + 1;
        }
        Employee employee = new Employee(nextEmpID, fName, lName);
        employees.add(employee);
    }

    public void deleteEmployeeByID(int ID) {
        employees.removeIf(employee -> employee.getEmpID() == ID);
    }

    public void getListOfEmployees() {
        if (!employees.isEmpty()) {
            employees.forEach((employee) -> System.out.println(employee.toString()));
        } else {
            System.out.println("The list is empty...");
        }
    }
}
