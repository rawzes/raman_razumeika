package com.rr.training.hw6.task1.models;

public class DebitCard extends Card {
    public DebitCard(String ownerName, double balance) {
        super(ownerName, balance);
    }

    public DebitCard(String ownerName) {
        super(ownerName);
    }

    @Override
    public void reduceBalance(double sum) throws AtmExceptions {

            if (this.balance - sum < 0) {
                throw new AtmExceptions("The balance cannot be < 0");
            } else {
                this.balance -= sum;
            }

    }
}
