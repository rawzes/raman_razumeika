package com.rr.training.hw6.task1;

import com.rr.training.hw6.task1.models.Atm;
import com.rr.training.hw6.task1.models.Card;
import com.rr.training.hw6.task1.models.CreditCard;
import com.rr.training.hw6.task1.models.DebitCard;

public class Runner {
    public static void main(String[] args) {

        Card c1 = new DebitCard("Roma", 500);
        Card c2 = new CreditCard("Tanya", 300);
        Atm belBank = new Atm(c2);
        try {
            belBank.reduceBalance(506);
            System.out.println(c2.getBalance());
        } catch (Exception e) {
            System.out.println(e.getStackTrace());
        }

    }
}
