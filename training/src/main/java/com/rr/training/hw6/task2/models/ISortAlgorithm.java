package com.rr.training.hw6.task2.models;

public interface ISortAlgorithm {
    void sort(int[] array);
}
