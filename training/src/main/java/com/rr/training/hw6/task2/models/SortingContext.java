package com.rr.training.hw6.task2.models;

public class SortingContext {
    private ISortAlgorithm sortStrategy;

    public SortingContext(ISortAlgorithm sortStrategy) {
        this.sortStrategy = sortStrategy;
    }

    public void setSortAlgorithm(ISortAlgorithm sortStrategy) {
        this.sortStrategy = sortStrategy;
    }

    public void execute(int[] arr) {
        this.sortStrategy.sort(arr);
    }
}
