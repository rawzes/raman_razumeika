package com.rr.training.hw6.task1.models;

public class Atm {

    private Card internalCard;

    public Atm(Card card) {
        this.internalCard = card;
    }

    public void reduceBalance(double sum) throws AtmExceptions {
        this.internalCard.reduceBalance(sum);
    }

    public void addBalance(double sum) {
        this.internalCard.addBalance(sum);
    }

}
