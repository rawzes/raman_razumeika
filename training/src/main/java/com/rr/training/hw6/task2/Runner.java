package com.rr.training.hw6.task2;


import com.rr.training.hw6.task2.models.BubbleSort;
import com.rr.training.hw6.task2.models.ISortAlgorithm;
import com.rr.training.hw6.task2.models.SelectionSort;
import com.rr.training.hw6.task2.models.SortingContext;

import java.util.Arrays;
import java.util.Scanner;

public class Runner {
    public static void main(String[] args) {
        int[] array = new int[]{-1, 434, 4, 2, 6, -29};
        ISortAlgorithm sortStrategy = selectSortStrategy(selectSortID());
        SortingContext context = new SortingContext(sortStrategy);
        context.execute(array);
        System.out.println(Arrays.toString(array));
    }

    private static ISortAlgorithm selectSortStrategy(int algorithmID) {
        switch (algorithmID) {
            case 1:
                return new BubbleSort();
            case 2:
                return new SelectionSort();
            default:
                return new SelectionSort();
        }
    }

    private static int selectSortID() {
        Scanner scanner = new Scanner(System.in);
        int algorithmID = 1;
        System.out.println("Menu");
        System.out.println("1. Select Bubble sort");
        System.out.println("2. Select Selection sort");

        System.out.print("Please enter either 1 to 2: ");
        try {
            algorithmID = Integer.parseInt(scanner.nextLine());
        } catch (Exception e) {
            System.out.println("Please, enter correct number...");
        }
        return algorithmID;
    }
}
