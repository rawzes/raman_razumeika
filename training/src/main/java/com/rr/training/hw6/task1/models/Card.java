package com.rr.training.hw6.task1.models;

public class Card {
    protected String ownerName;
    protected double balance;

    public Card(String ownerName, double balance) {
        this.ownerName = ownerName;
        this.balance = balance;
    }

    public Card(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public double getBalance() {
        return this.balance;
    }

    public void addBalance(double sum) {
        if (sum < 0) {
            throw new IllegalArgumentException();
        } else {
            this.balance += sum;
        }

    }

    public void reduceBalance(double sum) throws AtmExceptions {
        this.balance -= sum;
    }

    public double convertToEUR(double course) {
        return this.balance * course;
    }

    public double convertToBYN(double course) {
        return this.balance * course;
    }
}