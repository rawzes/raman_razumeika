package com.rr.training.hw6.task1.models;

public class CreditCard extends Card {
    public CreditCard(String ownerName, double balance) {
        super(ownerName, balance);
    }

    public CreditCard(String ownerName) {
        super(ownerName);
    }
}
