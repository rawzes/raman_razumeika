create table Employees (id integer primary key, firstname text, lastname text);

insert into Employees(firstname, lastname) values('Ivan', 'Ivanov');
insert into Employees(firstname, lastname) values('Petr', 'Petrov');
insert into Employees(firstname, lastname) values('Ivan', 'Sidorov');