package com.rr.training.hw6.task1;

import com.rr.training.hw6.task1.models.*;
import org.junit.Assert;
import org.junit.Test;

public class AtmTest {

    @Test
    public void testAtmCreationConstructor() {
        CreditCard card = new CreditCard("Test", 300);
        Atm atm1 = new Atm(card);
        Assert.assertNotNull("The instance was not created", atm1);
    }

    @Test
    public void testAtmReduceDebitCard() throws AtmExceptions {
        double sum = 200;
        Card card = new DebitCard("Test", sum);
        Atm atm1 = new Atm(card);
        atm1.reduceBalance(100);
        Assert.assertEquals("Incorrect value of balance after reducing DebitCard", sum - 100, card.getBalance(), 0);
    }

    @Test
    public void testAtmReduceCreditCard() throws AtmExceptions {
        double sum = 200;
        Card card = new CreditCard("Test", sum);
        Atm atm1 = new Atm(card);
        atm1.reduceBalance(100);
        Assert.assertEquals("Incorrect value of balance after reducing CreditCard", sum - 100, card.getBalance(), 0);
    }

    @Test
    public void testAtmAddBalanceToCreditCard() {
        double sum = 200;
        Card card = new CreditCard("Test", sum);
        Atm atm1 = new Atm(card);
        atm1.addBalance(100);
        Assert.assertEquals("Incorrect value of balance after adding balance to CreditCard", sum + 100, card.getBalance(), 0);
    }

    @Test
    public void testAtmAddBalanceToDebitCard() {
        double sum = 200;
        Card card = new DebitCard("Test", sum);
        Atm atm1 = new Atm(card);
        atm1.addBalance(100);
        Assert.assertEquals("Incorrect value of balance after adding balance to DebitCard", sum + 100, card.getBalance(), 0);
    }

    @Test(expected = AtmExceptions.class)
    public void testAtmReduceAllBalanceFromDebitCard() throws AtmExceptions {
        double sum = 200;
        Card card = new DebitCard("Test", sum);
        Atm atm1 = new Atm(card);
        atm1.reduceBalance(sum + 1000);
    }

    @Test
    public void testAtmReduceAllBalanceFromCreditCard() throws AtmExceptions {
        double sum = 200;
        Card card = new CreditCard("Test", sum);
        Atm atm1 = new Atm(card);
        atm1.reduceBalance(1000);
        Assert.assertEquals("Incorrect balance after reducing balance from CreditCard", sum - 1000, card.getBalance(), 0);
    }
}
