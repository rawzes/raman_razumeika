package com.rr.training.hw5.task1;

import org.junit.Assert;
import org.junit.Test;

public class CardTest {

    @Test
    public void testConstructorWith1Parameter() {
        String testName = "TestName";
        Card card = new Card(testName);
        Assert.assertNotNull("Instance is not created", card);
    }

    @Test
    public void testConstructorWith2Parameters() {
        String testName = "TestName";
        double sum = 404.40;
        Card card = new Card(testName, sum);
        Assert.assertNotNull("Instance is not created", card);
    }

    @Test
    public void testGetBalance() {
        String testName = "TestName";
        double sum = 404.40;
        Card card = new Card(testName, sum);
        Assert.assertEquals("Incorrect balance value", sum, card.getBalance(), 0);
    }

    @Test
    public void testGetOwnerName() {
        String testName = "TestName";
        Card card = new Card(testName);
        Assert.assertEquals("Incorrect name value", testName, card.getOwnerName());
    }

    @Test
    public void testAddBalance() {
        String testName = "TestName";
        double sum = 404.40;
        double delta = 10;
        Card card = new Card(testName, sum);
        card.addBalance(delta);
        Assert.assertEquals("Incorrect balance after AddBalance method", sum+delta, card.getBalance(), 0);
    }

    @Test
    public void testReduceBalance() {
        String testName = "TestName";
        double sum = 404.40;
        double delta = 10;
        Card card = new Card(testName, sum);
        card.reduceBalance(delta);
        Assert.assertEquals("Incorrect balance after ReduceBalance method", sum-delta, card.getBalance(), 0);
    }

    @Test
    public void testConvertToEUR() {
        String testName = "TestName";
        double sum = 500;
        double coefficient = 0.89;
        Card card = new Card(testName, sum);
        Assert.assertEquals("Incorrect sum after converting to EUR method", sum*coefficient, card.convertToEUR(coefficient), 0);
    }

    @Test
    public void testConvertToBYN() {
        String testName = "TestName";
        double sum = 500;
        double coefficient = 2.08;
        Card card = new Card(testName, sum);
        Assert.assertEquals("Incorrect sum after converting to BYN method", sum*coefficient, card.convertToBYN(coefficient), 0);
    }

    @Test (expected = IllegalArgumentException.class)
    public void testReduceBalanceWithMoreThanCurrentBalanceValue() {
        String testName = "TestName";
        double sum = 500;
        double delta = 1000;
        Card card = new Card(testName, sum);
        card.reduceBalance(delta);
    }

    @Test (expected = IllegalArgumentException.class)
    public void testAddBalanceWithNegativeValue() {
        String testName = "TestName";
        double sum = 500;
        double delta = -5;
        Card card = new Card(testName, sum);
        card.addBalance(delta);
    }
}
