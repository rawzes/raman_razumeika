package com.rr.automation.hw1;

import com.epam.tat.module4.Calculator;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class TestCalculator {

    private Calculator calculator;

    @BeforeMethod(alwaysRun = true)
    public void setUpCalculator() {
        calculator = new Calculator();
    }

    @DataProvider(name = "TestDoubleProvider")
    public Object[][] createTestDoubleValues() {
        return new Object[][]{
                {0.0, 0.0}, {Double.MAX_VALUE, Double.MAX_VALUE,}, {Math.PI, 10.00001}, {10.00001, Math.PI}
        };
    }

    @DataProvider(name = "TestLongProvider")
    public Object[][] createTestLongValues() {
        return new Object[][]{
                {0, 0}, {Long.MAX_VALUE, Long.MAX_VALUE,}, {Long.MIN_VALUE, 99999}, {10, 999999999}
        };
    }

    @Test(groups = {"Smoke", "Regression"}, dataProvider = "TestLongProvider", priority = 1)
    public void testCalculatorSumLong(long val1, long val2) {
        long actualResult = calculator.sum(val1, val2);
        assertEquals(actualResult, val1 + val2, "Incorrect result in sum method");
    }

    @Test(groups = "Regression", dataProvider = "TestDoubleProvider", priority = 2)
    public void testCalculatorSumDouble(double val1, double val2) {
        double actualResult = calculator.sum(val1, val2);
        assertEquals(actualResult, val1 + val2, "Incorrect result in sum method");
    }

    @Test(groups = {"Smoke", "Regression"}, dataProvider = "TestLongProvider", priority = 1)
    public void testCalculatorSubLong(long val1, long val2) {
        long actualResult = calculator.sub(val1, val2);
        assertEquals(actualResult, val1 - val2, "Incorrect result in sub method");
    }

    @Test(groups = "Regression", dataProvider = "TestDoubleProvider", priority = 2)
    public void testCalculatorSubDouble(double val1, double val2) {
        double actualResult = calculator.sub(val1, val2);
        assertEquals(actualResult, val1 - val2, "Incorrect result in sub method");
    }

    @Test(groups = {"Smoke", "Regression"}, dataProvider = "TestLongProvider", priority = 1)
    public void testCalculatorMultLong(long val1, long val2) {
        long actualResult = calculator.mult(val1, val2);
        assertEquals(actualResult, val1 * val2, "Incorrect result in mult method");
    }

    @Test(groups = "Regression", dataProvider = "TestDoubleProvider", priority = 2)
    public void testCalculatorMultDouble(double val1, double val2) {
        double actualResult = calculator.mult(val1, val2);
        assertEquals(actualResult, val1 * val2, "Incorrect result in mult method");
    }


    @Test(groups = {"Smoke", "Regression"})
    public void testCalculatorDivLong() {
        long testValue = 10;
        long actualResult = calculator.div(testValue, testValue);
        assertEquals(actualResult, testValue / testValue, "Incorrect result in div method");
    }

    @Test(groups = "Regression", expectedExceptions = NumberFormatException.class)
    public void testCalculatorDivLong2() {
        long testValue = 10;
        long actualResult = calculator.div(testValue, 0);
        assertEquals(actualResult, testValue / 0, "Number format exception in div method");
    }

    @Test(groups = "Regression", dataProvider = "TestDoubleProvider")
    public void testCalculatorDivDouble(double val1, double val2) {
        double actualResult = calculator.div(val1, val2);
        assertEquals(actualResult, val1 / val2, "Incorrect result in div method");
    }

    @Test(groups = "Regression", dataProvider = "TestDoubleProvider")
    public void testCalculatorPow(double val1, double val2) {
        double actualResult = calculator.pow(val1, val2);
        assertEquals(actualResult, Math.pow(val1, val2), "Incorrect result in pow method");
    }

    @Test(groups = "Regression")
    public void testCalculatorSqrt() {
        double testValue = 81;
        double actualResult = calculator.sqrt(testValue);
        assertEquals(actualResult, Math.sqrt(testValue), "Incorrect result in sqrt method");
    }

    @Test(groups = "Regression")
    public void testCalculatorTg() {
        double testValue = 60;
        double actualResult = calculator.tg(testValue);
        assertEquals(actualResult, Math.tan(testValue), "Incorrect result in tg method");
    }

    @Test(groups = "Regression")
    public void testCalculatorCtg() {
        double testValue = 60;
        double actualResult = calculator.ctg(testValue);
        assertEquals(actualResult, Math.cos(testValue) / Math.sin(testValue), "Incorrect result in ctg method");
    }

    @Test(groups = "Regression")
    public void testCalculatorCos() {
        double testValue = 60;
        double actualResult = calculator.cos(testValue);
        assertEquals(actualResult, Math.cos(testValue), "Incorrect result in cos method");
    }

    @Test(groups = "Regression")
    public void testCalculatorSin() {
        double testValue = 60;
        double actualResult = calculator.sin(testValue);
        assertEquals(actualResult, Math.sin(testValue), "Incorrect result in sin method");
    }

    @Test(groups = "Regression")
    public void testCalculatorIsPositive() {
        long testValue = 60;
        boolean actualResult = calculator.isPositive(testValue);
        Assert.assertTrue(actualResult, "Incorrect result in IsPositive() method");
    }

    @Test(groups = "Regression")
    public void testCalculatorIsNegative() {
        long testValue = 60;
        boolean actualResult = calculator.isNegative(testValue);
        Assert.assertFalse(actualResult, "Incorrect result in IsNegative() method");
    }

    @AfterMethod
    public void tearDownCalculator() {
        calculator = null;
    }
}
