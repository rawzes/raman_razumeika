package com.rr.automation.hw2;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.time.Instant;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class GmailTest {
    private static final String CHROME_PATH = "src/test/resources/chromedriver.exe";
    private static final String BODY = "testBody";
    private static WebDriver driver;
    private String propertyPath = "src/test/resources/config.properties";
    private Properties properties;

    public static void waitForElementVisibility(By locator) {
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    @AfterClass
    public static void tearDown() {
        driver.quit();
    }

    @BeforeClass
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", CHROME_PATH);
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }

    @Test
    public void testMailService() throws Exception {
        properties = getConfigProperties(propertyPath);
        driver.get(properties.getProperty("config.baseurl"));

        driver.findElement(By.name("identifier")).sendKeys(properties.getProperty("config.login"));
        String nextButton1 = "#identifierNext span";
        waitForElementVisibility(By.cssSelector(nextButton1));
        driver.findElement(By.cssSelector(nextButton1)).click();

        waitForElementVisibility(By.name("password"));
        driver.findElement(By.name("password")).sendKeys(properties.getProperty("config.password"));
        String nextButton2 = "#passwordNext span";
        waitForElementVisibility(By.cssSelector(nextButton2));
        driver.findElement(By.cssSelector(nextButton2)).click();

        Assert.assertTrue(driver.findElement(By.cssSelector("a[href^='https://accounts.google.com/SignOutOptions']")).isDisplayed(), "User is not logged in the system");

        String writeButton = "//div[@jscontroller]//div[@role='button']";
        waitForElementVisibility(By.xpath(writeButton));
        driver.findElement(By.xpath(writeButton)).click();

        Instant instant = Instant.now();
        String subject = String.valueOf(instant.toEpochMilli());

        waitForElementVisibility(By.name("to"));
        driver.findElement(By.name("to")).sendKeys(properties.getProperty("config.addressee"));
        driver.findElement(By.name("subjectbox")).sendKeys(subject);
        String selectorBody = "//div[@role='textbox' and contains(@class,'editable')]";
        driver.findElement(By.xpath(selectorBody)).sendKeys(BODY);
        waitForElementVisibility(By.xpath("//img[@data-tooltip][3]"));
        driver.findElement(By.xpath("//img[@data-tooltip][3]")).click();


        driver.findElement(By.cssSelector("a[href$='drafts']")).click();
        String draftMail = "//div[@role='link']//span[@data-legacy-standalone-draft-id][1]";
        waitForElementVisibility(By.xpath(draftMail));
        driver.findElement(By.xpath(draftMail)).click();

        String actualTo = "//div[@tabindex='1']//span";
        waitForElementVisibility(By.xpath(actualTo));
        WebElement actualSendTo = driver.findElement(By.xpath(actualTo));
        Assert.assertEquals(actualSendTo.getText(), properties.getProperty("config.addressee"), "Wrong email address was saved");

        String actualSubject = driver.findElement(By.cssSelector("input[name='subjectbox']")).getAttribute("value");
        Assert.assertEquals(actualSubject, subject, "Wrong subject was saved");
        String actualBody = driver.findElement(By.xpath(selectorBody)).getText();
        Assert.assertEquals(actualBody, BODY, "Wrong body was saved");

        driver.findElement(By.cssSelector("table[role='group'] div[role='button']:nth-of-type(1)")).click();
        Assert.assertEquals(driver.findElements(By.xpath(String.format("//td[@tabindex='-1']//span[contains(text(), '%s')]", subject))).size(), 0, "The mail is not deleted from drafts");

        driver.findElement(By.cssSelector("a[href$='sent']")).click();
        Assert.assertTrue(driver.findElement(By.xpath(String.format("//td[@tabindex='-1']//span[contains(text(), '%s')]", subject))).isDisplayed(), "The mail is not displayed in Sents folder");

        driver.findElement(By.cssSelector("a[href*='accounts.google.com/SignOutOptions']")).click();
        driver.findElement(By.cssSelector("a[href*='accounts.google.com/Logout")).click();
    }

    private Properties getConfigProperties(String propertyPath) {
        BufferedReader reader;
        Properties properties;
        try {
            reader = new BufferedReader(new FileReader(propertyPath));
            properties = new Properties();
            try {
                properties.load(reader);
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException("Configuration.properties not found at " + propertyPath);
        }
        return properties;
    }
}
