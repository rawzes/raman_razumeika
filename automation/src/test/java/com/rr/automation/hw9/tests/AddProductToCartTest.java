package com.rr.automation.hw9.tests;

import com.rr.automation.hw9.browser.Browser;
import com.rr.automation.hw9.utils.CustomRunner;
import cucumber.api.CucumberOptions;
import org.junit.AfterClass;
import org.junit.runner.RunWith;

@RunWith(CustomRunner.class)
@CucumberOptions(
        plugin = {"pretty"},
        monochrome = true,
        strict = true,
        tags = "@card",
        glue = {"com.rr.automation.hw9.steps"},
        features = {"src/test/resources/com.rr.automation.hw9/addToCard.feature"}
)
public class AddProductToCartTest {
    @AfterClass
    public static void closeBrowser() {
        Browser.close();
    }
}
