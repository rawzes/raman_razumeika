package com.rr.automation.hw9.utils;

import com.rr.automation.hw9.browser.Browser;
import cucumber.api.junit.Cucumber;
import org.junit.runner.notification.RunNotifier;
import org.junit.runners.model.InitializationError;
import org.openqa.selenium.TakesScreenshot;

import java.io.IOException;

public class CustomRunner extends Cucumber {
    public CustomRunner(Class clazz) throws InitializationError, IOException {
        super(clazz);
    }

    @Override
    public void run(RunNotifier notifier) {
        notifier.addListener(new CustomListener((TakesScreenshot) Browser.getInstance()));
        super.run(notifier);
    }
}
