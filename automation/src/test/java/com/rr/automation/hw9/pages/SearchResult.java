package com.rr.automation.hw9.pages;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;

public class SearchResult extends Page {
    private By listProducts = By.cssSelector("ul.srp-results h3.s-item__title");
    private By productsLinks = By.cssSelector("a.s-item__link");
    private Logger logger = LogManager.getLogger(SearchResult.class);

    private String getTitleOfFirstSerchItem() {
        logger.debug("Get text of product: " + driver.findElements(listProducts).get(0).getText());
        return driver.findElements(listProducts).get(0).getText();
    }

    public boolean isTitleContainsKeyWord(String text) {
        logger.debug("Check is title contains key word: " + getTitleOfFirstSerchItem().toLowerCase());
        return getTitleOfFirstSerchItem().toLowerCase().contains(text.toLowerCase());
    }

    public ProductPage openProductPage() {
        logger.info("Find product link: " + driver.findElements(productsLinks).get(1).getText());
        driver.findElements(productsLinks).get(1).click();
        return new ProductPage();
    }
}
