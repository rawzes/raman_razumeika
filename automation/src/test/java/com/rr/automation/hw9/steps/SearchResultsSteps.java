package com.rr.automation.hw9.steps;

import com.rr.automation.hw9.pages.HomePage;
import com.rr.automation.hw9.pages.SearchResult;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import static org.junit.Assert.assertTrue;

public class SearchResultsSteps {
    private Logger logger = LogManager.getLogger(AddProductToCartSteps.class);
    @Given("^I opened Home Page$")
    public void iOpenedHomePage() {
        logger.debug("Opened Home Page");
        new HomePage().openHomePage();
    }

    @When("^I search the product \"([^\"]*)\"$")
    public void iSearchTheProduct(String text) throws Throwable {
        logger.debug("Submit search request: " + text);
        new HomePage().typeValueInSearchField(text).searchSubmit();
    }


    @Then("^the term query \"([^\"]*)\" should be the first in the Search Result grid$")
    public void theTermQueryShouldBeTheFirstInTheSearchResultGrid(String option) throws Throwable {
        logger.debug("Checking availability of element: " + option);
        assertTrue(new SearchResult().isTitleContainsKeyWord(option));
    }

}
