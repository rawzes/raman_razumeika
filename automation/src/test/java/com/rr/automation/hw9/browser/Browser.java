package com.rr.automation.hw9.browser;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class Browser {
    private static WebDriver driver;
    private static Logger logger = LogManager.getLogger(Browser.class);
    public static WebDriver getInstance() {
        if (driver != null) {
            return driver;
        } else {
            return driver = initDriver();
        }
    }

    private static WebDriver initDriver() {
        logger.info("Initialization of webdriver");
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        logger.debug("New instance of driver was created");
        return driver;
    }

    public static void close() {
        try {
            logger.info("Driver quit command");
            driver.quit();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            driver = null;
        }
    }

    public static String getCurrentUrl() {
        logger.debug("Current URL is " + driver.getCurrentUrl());
        return driver.getCurrentUrl();
    }
}
