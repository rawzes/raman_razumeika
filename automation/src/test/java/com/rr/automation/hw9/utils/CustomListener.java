package com.rr.automation.hw9.utils;

import org.junit.runner.Description;
import org.junit.runner.notification.RunListener;
import org.openqa.selenium.TakesScreenshot;

public class CustomListener extends RunListener {

    private TakesScreenshot screenshotTaker;

    public CustomListener(TakesScreenshot screenshotTaker) {
        this.screenshotTaker = screenshotTaker;
    }

    @Override
    public void testFinished(Description description) throws Exception {
        System.out.println("Getting a screenshot for test: " + description.getDisplayName());
        DriverUtil.saveScreenShot(screenshotTaker);
    }
}
