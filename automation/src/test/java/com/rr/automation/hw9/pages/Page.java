package com.rr.automation.hw9.pages;

import com.rr.automation.hw9.browser.Browser;
import org.apache.log4j.LogManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.logging.Logger;

public abstract class Page {
    protected final String BASE_URL = "https://www.ebay.com/";
    protected WebDriver driver = Browser.getInstance();

    public void waitForElementVisible(By locator) {
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOfElementLocated(locator));
    }
}
