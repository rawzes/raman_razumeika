package com.rr.automation.hw9.pages;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;

public class HomePage extends Page {

    private By searchField = By.cssSelector("table[role='presentation'] input.ui-autocomplete-input");
    private By searchButton = By.cssSelector("#gh-btn");
    private Logger logger = LogManager.getLogger(HomePage.class);

    public HomePage openHomePage() {
        logger.debug("Open Home page: " + BASE_URL);
        driver.get(BASE_URL);
        return this;
    }

    public HomePage typeValueInSearchField(String value) {
        driver.findElement(searchField).clear();
        logger.debug("Type value in search field: " + value);
        driver.findElement(searchField).sendKeys(value);
        return this;
    }

    public SearchResult searchSubmit() {
        logger.info("Click search button");
        driver.findElement(searchButton).click();
        return new SearchResult();
    }
}
