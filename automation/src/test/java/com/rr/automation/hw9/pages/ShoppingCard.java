package com.rr.automation.hw9.pages;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;

public class ShoppingCard extends Page {

    private By listAddedProducts = By.cssSelector("div.listsummary-content-itemdetails span.BOLD");
    private Logger logger = LogManager.getLogger(ShoppingCard.class);

    public boolean isCartHasAddedProduct(String text) {
        logger.debug("Check added product in card: " +  getTitleOfFirstSerchItem().toLowerCase());
        return getTitleOfFirstSerchItem().toLowerCase().contains(text.toLowerCase());
    }

    private String getTitleOfFirstSerchItem() {
        return driver.findElements(listAddedProducts).get(0).getText();
    }
}
