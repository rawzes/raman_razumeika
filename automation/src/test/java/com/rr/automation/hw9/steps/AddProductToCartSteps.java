package com.rr.automation.hw9.steps;

import com.rr.automation.hw9.pages.ProductPage;
import com.rr.automation.hw9.pages.SearchResult;
import com.rr.automation.hw9.pages.ShoppingCard;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Assert;

public class AddProductToCartSteps {
    private Logger logger = LogManager.getLogger(AddProductToCartSteps.class);

    @And("^I opened product card$")
    public void iOpenedProductCard() {
        logger.debug("Opened product page");
        new SearchResult().openProductPage();
    }

    @And("^I click Add product to shopping cart button$")
    public void iClickAddProductToShoppingCartButton() {
        logger.debug("Add product to card step");
        new ProductPage().addProductToCard();
    }

    @Then("^the added item \"([^\"]*)\" should be the first in the Shopping cart$")
    public void theAddedItemShouldBeTheFirstInTheShoppingCart(String option) throws Throwable {
        logger.debug("Check availability of added product: " + option);
        Assert.assertTrue(new ShoppingCard().isCartHasAddedProduct(option));
    }
}
