package com.rr.automation.hw9.pages;

import com.rr.automation.hw9.utils.DriverUtil;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class ProductPage extends Page {

    private By addToCartButton = By.cssSelector("a#isCartBtn_btn");
    private Logger logger = LogManager.getLogger(ProductPage.class);
    public ShoppingCard addProductToCard() {
        logger.info("The element was highlighted");
        WebElement button = DriverUtil.highlightElement(addToCartButton);
        DriverUtil.saveScreenShot(driver);
        logger.info("Getting a screenshot");
        button.click();
        logger.info("Open Shopping Card page");
        return new ShoppingCard();
    }
}
