package com.rr.automation.hw9.utils;

import com.rr.automation.hw9.browser.Browser;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DriverUtil {

    public static void saveScreenShot(TakesScreenshot screenshotTaker) {
        File scrFile = screenshotTaker.getScreenshotAs(OutputType.FILE);
        getScreenshotName(scrFile);
    }

    public static void saveScreenShot(WebDriver driver) {
        File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        getScreenshotName(scrFile);
    }

    private static void getScreenshotName(File scrFile) {
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy-h-mm-ss-SS--a");
        String formattedDate = sdf.format(date);
        String fileName = "screenshot-" + formattedDate;
        try {
            String filePath = System.getProperty("screenshotPath");
            FileUtils.copyFile(scrFile, new File(String.format(filePath + "/%s.png", fileName)));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static WebElement highlightElement(By by) {

        WebElement elem = Browser.getInstance().findElement(by);

        if (Browser.getInstance() instanceof JavascriptExecutor) {

            ((JavascriptExecutor)Browser.getInstance()).executeScript("arguments[0].style.border='10px solid red'", elem);

        }
        return elem;
    }

}
