@card
Feature: Search products in store

  Scenario: Search product using a Full Text Quick Search
    Given I opened Home Page
    When I search the product "Dell"
    Then the term query "Dell" should be the first in the Search Result grid

  Scenario Outline: Search product using a Full Text Quick Search with parameters
    Given I opened Home Page
    When I search the product "<parameter>"
    Then the term query "<parameter>" should be the first in the Search Result grid
    Examples:
      | parameter         |
      | samsung galaxy s8 |
      | ASUS ZenBook      |
      | Acer Aspire       |