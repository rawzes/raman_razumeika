@card
Feature: Add product to card

  Scenario Outline: Search product using a Full Text Quick Search
    Given I opened Home Page
    When I search the product "<request>"
    And I opened product card
    And I click Add product to shopping cart button
    Then the added item "<request>" should be the first in the Shopping cart
    Examples:
      | request           |
      | samsung galaxy s8 |